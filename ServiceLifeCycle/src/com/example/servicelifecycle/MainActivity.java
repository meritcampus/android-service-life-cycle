package com.example.servicelifecycle;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Button startService = (Button) findViewById(R.id.start_service);
		Button stopService = (Button) findViewById(R.id.stop_service);
		startService.setOnClickListener(this);
		stopService.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		Intent intent=null;
		switch (v.getId()) {
		case R.id.start_service:
			 intent = new Intent(getApplicationContext(), MyService.class);
			startService(intent);
			break;
		case R.id.stop_service:
			 intent = new Intent(getApplicationContext(), MyService.class);
			stopService(intent);
			
			break;

		}

	}

}
